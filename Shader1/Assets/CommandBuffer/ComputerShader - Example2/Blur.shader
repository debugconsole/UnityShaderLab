﻿//Copy from CommandBuffer Demo.
Shader "Custom/Blur" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "" {}
		_BlurRadius("Blur Radius", float) = 0.2
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	struct v2f {
		float4 pos : POSITION;
		float2 uv : TEXCOORD0;

		float4 uv01 : TEXCOORD1;
		float4 uv23 : TEXCOORD2;
		float4 uv45 : TEXCOORD3;
	};

	float _BlurRadius;
	sampler2D _MainTex;

	#define BLUR_SCALE 0.03

	v2f vert(appdata_img v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);

		o.uv.xy = v.texcoord.xy;

		float4 offsets = _BlurRadius * BLUR_SCALE;
			
		o.uv01 = v.texcoord.xyxy + offsets.xyxy * float4(1, 1, -1, -1);
		o.uv23 = v.texcoord.xyxy + offsets.xyxy * float4(1, 1, -1, -1) * 2.0;
		o.uv45 = v.texcoord.xyxy + offsets.xyxy * float4(1, 1, -1, -1) * 3.0;

		return o;
	}

	half4 frag(v2f i) : COLOR{
		half4 color = float4 (0,0,0,0);

		color += 0.40 * tex2D(_MainTex, i.uv);
		color += 0.15 * tex2D(_MainTex, i.uv01.xy);
		color += 0.15 * tex2D(_MainTex, i.uv01.zw);
		color += 0.10 * tex2D(_MainTex, i.uv23.xy);
		color += 0.10 * tex2D(_MainTex, i.uv23.zw);
		color += 0.05 * tex2D(_MainTex, i.uv45.xy);
		color += 0.05 * tex2D(_MainTex, i.uv45.zw);

		return color;
	}

	ENDCG

	Subshader {
		Pass{

			Stencil
			{
				Ref 1
				Comp Equal
			}

			ZTest Always Cull Off ZWrite Off
			Fog{ Mode off }

			CGPROGRAM
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}

	Fallback off

} // shader
