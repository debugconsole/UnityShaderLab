﻿using System;
using UnityEngine;


/// <summary>
/// 新日志管理器 使用LuaInterface.Debugger来打印可以关闭的调试信息
///              使用UIDebugger来打印需要窗口显示的调试信息
/// </summary>
public class LogManager
{

    //UIDebug对象名称
    const string UIDebugPanel = "PanelDebug";

    //是否加上时间
    public static bool IsTimeEnabled { get; set; }

    //为了兼容老代码的单例
    static LogManager m_singleton = null;
    public static LogManager getSingleton()
    {
        if(m_singleton == null)
        {
            m_singleton = new LogManager();
            
        }

        return m_singleton;
    }


    static UIDebugger mUIDebugger = null;
    static bool isInit = false;
    static UIDebugger UIDebugger
    {
        get {
            if(!isInit)
            {
                isInit = true;
                GameObject debugLog = GameObject.Find(UIDebugPanel);
                if (debugLog == null)
                {
                    Debug.LogWarning("UI日志窗口未找到");
                    return null;
                }

                mUIDebugger = debugLog.GetComponent<UIDebugger>();
                mUIDebugger.gameObject.SetActive(true);
               
            }

            return mUIDebugger;

        }

    }
   

    public static void Log(string _strLogContent)
    {

        string strOutput = "";
        DateTime dt = DateTime.Now;

        if (IsTimeEnabled)
        {
            strOutput += dt.ToString();
        }

        strOutput += _strLogContent;

        //DEBUG输出
        Debug.Log(strOutput);

        if (UIDebugger != null)
        {
            UIDebugger.AddLog(strOutput);
        }
    }


    //错误输出
    public static void Err(string _strLogContent)
    {
        Debug.LogError(_strLogContent);

        if (UIDebugger != null)
        {
            UIDebugger.AddLog("Err:" + _strLogContent);
        }

    }


    //警告输出
    public static void War(string _strLogContent)
    {
        Debug.LogWarning(_strLogContent);

        if (UIDebugger != null)
        {
            UIDebugger.AddLog("Warning:" + _strLogContent);
        }

    }




    ////普通日志输出
    //public void Log(string _strLogContent)
    //{
    //    string strOutput= "";
    //    DateTime dt = DateTime.Now;

    //    if (m_bIsTimeEnabled)
    //    {
    //        strOutput += dt.ToString();
    //    }

    //    strOutput += _strLogContent;

    //    //DEBUG输出
    //    Debug.Log(strOutput);

    //    if(m_UIDebugger!=null)
    //    {
    //        m_UIDebugger.AddLog(strOutput);
    //    }


    //}

    ////错误输出
    //public void Err(string _strLogContent)
    //{
    //    Debug.LogError(_strLogContent);

    //    if (m_UIDebugger != null)
    //    {
    //        m_UIDebugger.AddLog("Err:" + _strLogContent);
    //    }

    //}


    ////警告输出
    //public void War(string _strLogContent)
    //{
    //    Debug.LogWarning(_strLogContent);

    //    if (m_UIDebugger != null)
    //    {
    //        m_UIDebugger.addLog("Warning:" + _strLogContent);
    //    }

    //}

}

