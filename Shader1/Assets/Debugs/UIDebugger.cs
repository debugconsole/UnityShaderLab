﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;



/// <summary>
/// 可以异步输出日志到界面的日志脚本
/// 绑定到UI日志对象ROOT上否则隐藏功能不好用。
/// </summary>
public class UIDebugger: MonoBehaviour {

    //调试窗口名称
    public const string DEBUGWINDOWNAME = "DebugWnd";

    //内容Text对象
    [SerializeField]
    GameObject m_contentObj;
    //清理内容
    [SerializeField]
    Button ref_clearBtn;
    //隐藏面板(挂在哪个对象上隐藏哪个)
    [SerializeField]
    Button ref_hideBtn;

    //剖析信息
    [SerializeField]
    Text ref_fpsText;   //关联剖析信息UI Text对象

    int m_count = 0;
    int m_logLength = 0;
    bool m_bIsUpdate = false;
    string m_strLog="";
    const int MAX_LOG_NUM = 50;

    // Use this for initialization
    void Start () {
        getScreenInfo();
        ref_clearBtn.onClick.AddListener(Clear);
        ref_hideBtn.onClick.AddListener(SetEnabled);
    }
	
	// Update is called once per frame
	void Update () {

        //更新剖析信息
        UpdateProfileInfo();

        if (m_bIsUpdate)
        {
            //更新日志
            Text textObj = m_contentObj.GetComponent<Text>();
            textObj.text = m_strLog;
            m_count++;
            RectTransform selfTransform = m_contentObj.GetComponent<RectTransform>();
            selfTransform.sizeDelta = new Vector2(0, textObj.preferredHeight);  //preferredHeight 取TEXT实际大小
            m_bIsUpdate = false;
        }
	}

    string screenInfo;
    void getScreenInfo()
    {
        screenInfo = "w:" + Screen.width + " h:" + Screen.height;
    }


    const string STRENDPOSTFIX = "&\r\n";
    const string STRPREFIX = "-->";
    //添加一条日志 永远显示在最上面
    public void AddLog(string _logMsg)
    {

        if (m_count > MAX_LOG_NUM)
        {
            //m_strLog = m_strLog.Substring(0, (int)(m_strLog.Length * 0.5));
            m_strLog = m_strLog.Substring(0, (int)m_strLog.LastIndexOf(STRPREFIX));
            m_count --;
        }

        //m_logLength = _logMsg.Length + m_logLength

        m_strLog = m_count + STRPREFIX + _logMsg + STRENDPOSTFIX + m_strLog;

        m_bIsUpdate = true;

    }


    //清理
    void Clear()
    {
        if(m_bIsUpdate == false)
        {
            m_strLog = "";
            m_count = 0;
            m_bIsUpdate = true;
        }

    }

    GameObject debuggerView = null;
    void SetEnabled()
    {
        if(debuggerView == null)
        {
            debuggerView = m_contentObj.transform.parent.parent.gameObject;
        }

        debuggerView.SetActive(!debuggerView.activeSelf);
    }

    public float interval = 1;
    string fps;
    string memory;
    string VT;
    float timeleft;
    //float accum;
    //float frames;

    //更新剖析信息
    void UpdateProfileInfo()
    {
        timeleft -= Time.deltaTime;
        //accum += Time.timeScale / Time.deltaTime;
        //++frames;
        if (timeleft < 0)
        {
            timeleft = interval;
            fps = (1f / Time.unscaledDeltaTime).ToString() ; // (accum / frames).ToString("f2") + "FPS";
            VT = GetObjectStats();
            memory = Profiler.GetTotalAllocatedMemory() / 1048576 + "MB / " + SystemInfo.systemMemorySize + "MB";
            ref_fpsText.text = fps + QualitySettings.currentLevel.ToString() + "\n" + screenInfo + "\n" + memory + "\n" + VT;
            //accum = 0;
            //frames = 0;
        }

    }



    int tris, verts, bones;
    string GetObjectStats()
    {
        SkinnedMeshRenderer[] filters;
        tris = verts = bones = 0;
        filters = GameObject.FindObjectsOfType<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer f in filters)
        {
            if (f.isVisible)
            {
                tris += f.sharedMesh.triangles.Length / 3;
                verts += f.sharedMesh.vertexCount;
                bones += f.bones.Length;
            }
        }
        //MeshFilter[] filters2;
        //filters2 = GameObject.FindObjectsOfType<MeshFilter>();
        //foreach (MeshFilter f in filters2)
        //{
        //    tris += f.mesh.triangles.Length / 3;
        //    verts += f.mesh.vertexCount;
        //}
        return "verts:" + verts / 1000f + "  tris:" + tris / 1000f + "  bones:" + bones;
    }

}
