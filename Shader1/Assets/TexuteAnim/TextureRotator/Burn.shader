﻿Shader "Unlit/Burn"
{

// 	注：
// _DissColor为溶解主色，_AddColor为叠加色，按照溶解的移动方向来看开始色为DissColor+AddColor
// 上图中DissColor为红色，AddColor为绿色
// 所以燃烧时
// 开始色为DissColor + AddColor = 黄色
// 默认色为DissColor 红色
// 然后配上火的粒子特效，这样就能模拟比较真实的燃烧效果。
	Properties
	{

		_Color ("主颜色", Color) = (1,1,1,1)                       // 主色  
        _MainTex ("模型贴图", 2D) = "white" {}                      // 主材质  
        _DissolveText ("溶解贴图", 2D) = "white" {}                 // 溶解贴图  
        _Tile("溶解贴图的平铺大小", Range (0, 1)) = 1                // 平铺值,设置溶解贴图大小  
  
        _Amount ("溶解值", Range (0, 1)) = 0.5                     // 溶解度  
        _DissSize("溶解大小", Range (0, 1)) = 0.1                   // 溶解范围大小  
  
        _DissColor ("溶解主色", Color) = (1,1,1,1)                  // 溶解颜色  
        _AddColor ("叠加色，与主色叠加为开始色[R|G|B>0表示启用]", Color) = (1,1,1,1) // 改色与溶解色融合形成开始色  
	}

    // SubShader {   
    //     Tags { "Queue"="Transparent"  "RenderType"="Transparent"  }  
    //     LOD 200  
    //     //Cull off  
      
    //     CGPROGRAM  
    //     #pragma target 3.0  
    //     #pragma surface surf BlinnPhong  
  
    //     sampler2D _MainTex;  
    //     sampler2D _DissolveText;  
    //     fixed4 _Color;          // 主色  
    //     half _Tile;             // 平铺值  
    //     half _Amount;           // 溶解度  
    //     half _DissSize;         // 溶解范围  
    //     half4 _DissColor;       // 溶解颜色  
    //     half4 _AddColor;        // 叠加色  
    //     // 最终色  
    //     static half3 finalColor = float3(1,1,1);  
  
    //     struct Input {  
    //         float2 uv_MainTex;  // 只需要主材质的UV信息  
    //     };  
  
    //     void surf (Input IN, inout SurfaceOutput o) {  
    //         // 对主材质进行采样  
    //         fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);  
    //         // 设置主材质和颜色  
    //         o.Albedo = tex.rgb * _Color.rgb;  
    //         // 对裁剪材质进行采样，取R色值  
    //         float ClipTex = tex2D (_DissolveText, IN.uv_MainTex/_Tile).r;  
  
    //         // 裁剪量 = 裁剪材质R - 外部设置量  
    //         float ClipAmount = ClipTex - _Amount;  
    //         if(_Amount > 0)  
    //         {  
    //             // 如果裁剪材质的R色值 < 设置的裁剪值  那么此点将被裁剪  
    //             if(ClipAmount < 0)  
    //             {  
    //                 clip(-0.1);  
    //             }  
    //             // 然后处理没有被裁剪的值  
    //             else  
    //             {  
    //                 // 针对没有被裁剪的点，【裁剪量】小于【裁剪大小】的做处理  
    //                 // 如果设置了叠加色，那么该色为ClipAmount/_DissSize(这样会形成渐变效果)  
    //                 if(ClipAmount < _DissSize)  
    //                 {  
    //                     if(_AddColor.x == 0)  
    //                         finalColor.x = _DissColor.x;  
    //                     else  
    //                         finalColor.x = ClipAmount/_DissSize;  
  
    //                     if (_AddColor.y == 0)  
    //                         finalColor.y = _DissColor.y;  
    //                     else  
    //                         finalColor.y = ClipAmount/_DissSize;  
            
    //                     if (_AddColor.z == 0)  
    //                         finalColor.z = _DissColor.z;  
    //                     else  
    //                         finalColor.z = ClipAmount/_DissSize;  
    //                     // 融合  
    //                     o.Albedo  = o.Albedo * finalColor * 2;  
    //                 }  
    //             }  
    //         }  
    //         o.Alpha = tex.a * _Color.a;  
    //     }  
    //     ENDCG  
    // }//endsubshader  


	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Cull off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;  
			sampler2D _DissolveText;  
			fixed4 _Color;          // 主色  
			half _Tile;             // 平铺值  
			half _Amount;           // 溶解度  
			half _DissSize;         // 溶解范围  
			half4 _DissColor;       // 溶解颜色  
			half4 _AddColor;        // 叠加色  
			float4 _MainTex_ST;
			// 最终色  
        	static half3 finalColor = float3(1,1,1);  
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// 设置主材质和颜色  
				float3 rgbColor = col.rgb * _Color.rgb; 
				// 对裁剪材质进行采样，取R色值  
            	float ClipTex = tex2D (_DissolveText, i.uv/_Tile).r;  
				// 裁剪量 = 裁剪材质R - 外部设置量  
				float ClipAmount = ClipTex - _Amount;  
				if(_Amount > 0)  
				{  
					// 如果裁剪材质的R色值 < 设置的裁剪值  那么此点将被裁剪  
					if(ClipAmount < 0)  
					{  
						clip(-0.1);  
					}  
					// 然后处理没有被裁剪的值  
					else  
					{  
						// 针对没有被裁剪的点，【裁剪量】小于【裁剪大小】的做处理  
						// 如果设置了叠加色，那么该色为ClipAmount/_DissSize(这样会形成渐变效果)  
						if(ClipAmount < _DissSize)  
						{  
							if(_AddColor.x == 0)  
								finalColor.x = _DissColor.x;  
							else  
								finalColor.x = ClipAmount/_DissSize;  
	
							if (_AddColor.y == 0)  
								finalColor.y = _DissColor.y;  
							else  
								finalColor.y = ClipAmount/_DissSize;  
				
							if (_AddColor.z == 0)  
								finalColor.z = _DissColor.z;  
							else  
								finalColor.z = ClipAmount/_DissSize;  
							// 融合  
							rgbColor  = rgbColor * finalColor * 2;  
						}  
					}  
				
				}  

				float4 retColor = float4(rgbColor.x,rgbColor.y,rgbColor.z,col.a*_Color.a );

				return retColor;
			}
			ENDCG
		}
	}
}
