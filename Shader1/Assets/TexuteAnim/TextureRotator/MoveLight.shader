﻿Shader "Unlit/MoveLight"
{
	Properties
	{
		_image ("image", 2D) = "white" {}   
        _widthRatio ("widthRatio", Range(0, 20)) = 1 //光线宽度系数，值越大效果越细  
        _percent ("percent(-1, 2)", Range(-1, 2)) = -1  
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		ZWrite Off     
		// 源rgb * a + 背景rgb * (1-源a)    
        Blend SrcAlpha OneMinusSrcAlpha

		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			sampler2D 	_image;
			float 		_widthRatio;
			float		_percent;

			struct v2f
			{
				float4 vPos : SV_POSITION;
				float2 uv : TEXCOORD0;		
			};

			float4 _image_ST;

			v2f vert (appdata_img v)
			{
				v2f o;
				o.vPos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv  = TRANSFORM_TEX( v.texcoord , _image); //v.texcoord.xy;
				return o;
			}
			
			float lastState = 0;

			fixed4 frag (v2f i) : COLOR0
			{
				// sample the texture
				fixed4 col = tex2D(_image, i.uv);

				//控制流光显示频率和速度
				float   delaytime = ( _Time.y * 1.5 ) % UNITY_PI;
				float 	_percent2 = lerp(-1.0, 2.0, delaytime);
				lastState = degrees(delaytime);
				if( lastState > 90 )
				{
					return  col;
				}
	

                // 正常情况UV值变大，当前点位置颜色会增强  
                // 下面用了lerp(1,0,f)，那么这里UV增大，当前点位置颜色会减弱  
                fixed2 blink_uv = (i.uv + _percent2)* _widthRatio - _widthRatio;  
                // 旋转矩阵，旋转30度，可以自己修改方向  
                fixed2x2 rotMat = fixed2x2(0.866,0.5,-0.5,0.866);   

                // 乘以旋转矩阵  
                blink_uv = mul(rotMat, blink_uv);  
                // 当y越靠近原点时，RGB值越大  
                // (1-f) * a + b * f;  
                // 起点f=0，颜色最亮；起点f=1，颜色最暗  
                fixed rgba = lerp(fixed(1),fixed(0),abs(blink_uv.y));  
                if(col.a > 0.05)  
                {  
					fixed retColor = saturate(rgba);
                    // 叠加RGB值，可以自己修改颜色  
                    col +=  retColor;
                }  
                return col;  
			}
			ENDCG
		}
	}

	Fallback Off
}
