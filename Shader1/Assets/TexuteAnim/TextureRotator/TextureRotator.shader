﻿Shader "Unlit/SDTextureRotator"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_RotateSpeed("Rotate Speed",Range(0,10))=1
	}
	SubShader
	{
		Tags {"Queue"="Transparent"  "RenderType"="Transparent"    }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _RotateSpeed;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX( v.uv , _MainTex);
				// float2 uv2 =  v.uv.xy - float2(0.5,0.5);
				// float angle = _RotateSpeed + _Time.y;
				// uv2 = ( uv2.x*cos(angle) - uv2.y*sin(angle) , uv2.x*sin(angle) + uv2.y*cos(angle));
				// //uv2 = ( (v.uv.x-0.5)*cos(angle) - (v.uv.y-0.5)*sin(angle) , (v.uv.x-0.5)*sin(angle) + (v.uv.y-0.5)*cos(angle));
				// uv2  += float2(0.5,0.5);
				// //uv2 = (cos(uv2.x-0.5) - sin(uv2.y - 0.5) ,sin(uv2.x-0.5) + cos(uv2.y - 0.5));
				// o.uv = TRANSFORM_TEX(uv2, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
			 	//定义一个float2来存储顶点的UV的XY,减去0.5是因为uv旋转的起始是1,1为中心，XY都减去0.5是把中心点移到中心。
                 float2 uv=i.uv.xy -float2(0.5,0.5);
                //        _Time   是一个内置的float4时间，X是1/20，Y是1倍，Z是2倍，W是3倍           
                //        旋转矩阵的公式是： COS() -  sin() , sin() + cos()     顺时针
                //                                             COS() +  sin() , sin() - cos()     逆时针 
			     float angle =    _RotateSpeed * _Time.y ;
                 uv = float2(uv.x*cos(angle) - uv.y*sin(angle),
                                        uv.x*sin(angle) + uv.y*cos(angle));

                 //再加回来
                 uv += float2(0.5,0.5);

				// sample the texture
				fixed4 col = tex2D(_MainTex, uv);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
